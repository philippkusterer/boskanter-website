class translation {
    constructor(en, fr, nl) {
        this.en = en;
        this.fr = fr;
        this.nl = nl;
    }
}

module.exports = {
    translation: translation
}